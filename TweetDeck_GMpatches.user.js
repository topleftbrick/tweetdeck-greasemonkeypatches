// ==UserScript==
// @name        TweetDeck Greasemonkey Patches
// @namespace   https://tweetdeck.twitter.com/
// @include     https://tweetdeck.twitter.com/*
// @version     0.1
// @grant       none
// ==/UserScript==

/*
# UTILITIES
*/
// Case-insensitive version of :contains
// Source: http://stackoverflow.com/questions/2196641/how-do-i-make-jquery-contains-case-insensitive-including-jquery-1-8
$.expr[':'].containsIgnoreCase = function (n, i, m) {
    return jQuery(n).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};


/*
# FILTERS
*/
$(document).on('DOMNodeInserted', function() {
    // Leave these here for any date-based filters.
    var now = new Date(); 
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var today = days[now.getDay()];

    // Date-based filters
    // For supported date formats see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse
    /*
    if (now < Date.parse('Sep 18, 2014 12:00:00')) {
        // Rules go in here
    }
    */

    // Day-based filters
    /*
    if (today == "Monday") {
        // Rules go in here
    }
    */

    // Disable auto-play of GIFs in main view. Click into tweets to see them.
    $('div.js-chirp-container video').removeAttr('autoplay');

});

//$(document).ready(function() {
//});

/*
# DOCUMENTATION
*/

// article elements have a data-account-key value which is *your* Twitter ID. Use for column/account targeting?
// Links in tweets (class .url-ext) have a "data-full-url" attribute. Might be used to target with domain regex, then bubble up to hide entire tweet.
// There's a span.column-number element in the column heds, but empty. See if value can be queried somehow.

// Find a :contains with regex:
// http://www.vikaskbh.com/jquery-contains-selector-regex-for-matching-text-within-elements/
// http://stackoverflow.com/questions/9309763/jquery-selector-contains-use-regular-expressions
// https://gist.github.com/Mottie/461488



// Target tweets by a specific user
// Can be used if people you follow keep retweeting a specific person you don't want to see
//$('.tweet-header:contains("@USERNICK")').closest('article').css('opacity', '0.5');

// Target retweets by a specific user
//$('.tweet-context:contains("DISPLAYNAME retweeted")').closest('article').css('opacity', '0.5');
// Target ALL retweets
// $('.tweet-context:contains(" retweeted")').closest('article').css('opacity', '0.5');


// Target string in tweet content, case-sensitive; VERY simple, eg "The" will match "Then" etc.
//$('.tweet-body:contains("TARGET-TEXT")').closest('article').css('opacity', '0.5');

// Target string in tweet content, NOT case-sensitive; VERY simple, eg "the" will match "other" etc.
//$('.tweet-body:containsIgnoreCase("TARGET-TEXT")').closest('article').css('opacity', '0.5');

// Hide all in-line media
// $('.media-preview').hide();


// Target tweets by specific user AND content
//$('article').find('.tweet-header:contains("@USERNICK")').siblings('.tweet-body:contains("TARGET-TEXT")').closest('article').css('opacity', '0.5');

// Date limits #working
// See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse for acceptable date formatting
//var now = new Date();
//if (now < Date.parse('Aug 15, 2014 13:30:00')) { Date format: Mon 01, 2000 24:00:00
//    $('.tweet-header:contains("@USERNICK")').closest('article').css('opacity', '0.5');
//}

// Target tweets by user action(activity column) #working
// Maybe should filter down to column type with .column-type-activity but there doesn't seem to be a way to grab a /specific/
// column of that type, if there are multiple.
//$('.activity-header:contains("DISPLAYNAME favorited")').closest('article').hide();
// It should be possible to do this using the @nick instead of display name(which makes this fragile), but it kept failing for some reason. Figure out.

// Highlight based on content #working but expand CSS compat
//$('.tweet-body:contains("TARGET-TEXT")').closest('article').css('background', '-moz-linear-gradient(top, #cf0404 0%, #ff3019 100%)');
// TODO?: Using above, maybe also bubble up and highlight the column header?

// Column limiting
// Target all "home" columns
//$('.column-type-home .tweet-text:contains("the")').closest('article').css('opacity', '0.5');

// Target a specific column by position (obviously this is sub-optimal if you rearrange/add columns a lot)
//$('.column:nth-child(2) .tweet-text:contains("the")').closest('article').css('opacity', '0.5');


// Results
// Hide
// .hide()
// Dim:
// .css('opacity', '0.5')
// Highlight:
// Gradient generator: http://www.css3factory.com/linear-gradients/ Just grab the linear-gradient line
// .css('background', 'linear-gradient(to bottom, #CF0404 0%, #FF3019 100%)')

/* COLUMN TARGETING
// It should be possible to add these to the front of any of the content-targeting rules. #untested
.column-type-home
.column-type-mention
.column-type-message
.column-type-scheduled (all accounts)
.column-type-interactions
.column-type-list
.column-type-favorite
.column-type-activity
# If the data-column attribute is persistent, we might be able to target specific ones with that. Investigate
*/ 